import sys
import dateparser
from datetime import datetime
from functools import partial
from typing import Dict, List, Callable, Optional, Any, Tuple


def pad(string: str, desired: int, padding_char: str = " "):
    if len(string) < desired:
        return padding_char*(desired-len(string)) + string
    else:
        return string


def pad_both_sides(string: str, desired: int, padding_char: str = " "):
    if len(string) < desired:
        padding_len = (desired - len(string)) / 2
        padding = padding_char * int(padding_len)
        string = padding + string + padding
        if padding_len != int(padding_len):
            string += padding_char
        return string
    else:
        return string


def stringify(s):
    if s is None:
        return ""
    else:
        return str(s)


class View:
    def __init__(self):
        self.offset = 5

    def show_table(self, title: str, table: Dict[str, List[Any]]):
        table = {col: list(map(stringify, values)) for col, values in table.items()}

        width = {}
        for key in table.keys():
            width[key] = max(map(len, table[key])) if len(table[key]) != 0 else 0
            width[key] = max(width[key], len(key))

        table_width = sum(width.values()) + self.offset*(len(table.keys())-1)
        print(pad_both_sides(" %s " % title, table_width, "-"))

        offset = " "*self.offset
        line = "-"*table_width

        print(offset.join([pad(key, width[key]) for key in table]))
        print(line)

        for row in zip(*list(table.values())):
            values = [pad(val, width[key]) for val, key in zip(row, table)]
            print(offset.join(values))

        print(line)

    def show_str(self, string: str):
        print(string)

    def show_error(self, error: str):
        print("ERROR: " + error)

    def read_int(self, prompt: str) -> Optional[int]:
        while True:
            print(prompt + " > ", end='', flush=True)
            input = sys.stdin.readline().strip()
            if input == "":
                return None
            try:
                return int(input)
            except ValueError:
                self.show_error("invalid number")
                continue

    def read_str(self, prompt: str) -> str:
        print(prompt + " > ", end='', flush=True)
        return sys.stdin.readline().strip()

    def read_date_range(self, prompt: str) -> Optional[Tuple[datetime, datetime]]:
        while True:
            print(prompt + " > ", end='', flush=True)
            line = sys.stdin.readline().strip()
            if line == "":
                return None
            iter = map(dateparser.parse, line.split("to"))
            start = next(iter)
            end = next(iter)
            if start is None or end is None:
                self.show_error("invalid date range")
                continue
            start = start.replace(microsecond=0)
            end = end.replace(microsecond=0)
            return start, end

    def read_bool(self, prompt: str) -> Optional[bool]:
        while True:
            print(prompt + " [Y/N] > ", end='', flush=True)
            line = sys.stdin.readline().strip()
            if line == "":
                return None
            elif line == "Y" or line == "y":
                return True
            elif line == "N" or line == "n":
                return False
            else:
                self.show_error("invalid boolean")
                continue

    def wait_for_command(self):
        while True:
            print("> ", end='', flush=True)
            command = sys.stdin.readline().strip()
            if command == "":
                continue

            desc = {
                "user": [("id", ""), ("name", "")],
                "video": [("id", ""), ("owner", "s"), ("playlist", "s"), ("public", "s"), ("date", "s")],
                "playlist": [("id", ""), ("owner", "s")]
            }

            for entity in desc:
                for action in ["create", "update", "delete"]:
                    if command == "%s %s" % (action, entity):
                        getattr(self, "%s_%s_callback" % (action, entity))()
                        return

                for field, suffix in desc[entity]:
                    if command == "show %s%s by %s" % (entity, suffix, field):
                        getattr(self, "show_%ss_callback" % entity)(field)
                        return
                if command == "show %ss" % entity:
                    getattr(self, "show_%ss_callback" % entity)(None)
                    return

            if command == "add video to playlist":
                self.add_playlist_video_callback()
                return
            if command == "remove video from playlist":
                self.remove_playlist_video_callback()
                return
            if command == "show users username not includes":
                self.show_users_callback("username not includes")
                return

            print("unknown command")

    def on(self, action: str, entity: str, callback):
        if action == "show":
            entity = entity + "s"
        setattr(self, "%s_%s_callback" % (action, entity), callback)

    def on_add_playlist_video(self, callback):
        self.add_playlist_video_callback = callback

    def on_remove_playlist_video(self, callback):
        self.remove_playlist_video_callback = callback
