import psycopg2
from datetime import datetime
from typing import Dict, List, Optional, Union, Tuple


User = Tuple[int, str, str]
Video = Tuple[int, str, int, str, bool, datetime]
Playlist = Tuple[int, str, int]


class Model:
    def __init__(self):
        self.conn = psycopg2.connect("dbname=Lab")
        self.cur = self.conn.cursor()

    def create_user(self, username: str, email: Optional[str]) -> int:
        if email is None:
            self.cur.execute("INSERT INTO users (username) VALUES (%s) RETURNING id;", (username,))
        else:
            self.cur.execute("INSERT INTO users (username, email) VALUES (%s, %s) RETURNING id;", (username, email))
        self.commit()

        return self.cur.fetchone()[0]

    def update_user_username(self, id: int, username: str):
        self.cur.execute("UPDATE users SET username = %s WHERE id = %s;", (username, id))
        self.commit()

    def update_user_email(self, id: int, email: Optional[str]):
        self.cur.execute("UPDATE users SET email = %s WHERE id = %s;", (email, id))
        self.commit()

    def read_users(self) -> List[User]:
        self.cur.execute("SELECT id, username, email FROM users;")
        return self.cur.fetchall()

    def read_user_by_id(self, id: int) -> User:
        self.cur.execute("SELECT id, username, email FROM users WHERE id = %s;", (id,))
        return self.cur.fetchone()

    def read_users_username_not_includes(self, word: str) -> List[User]:
        self.cur.execute("SELECT id, username, email FROM users WHERE NOT username::tsvector @@ %s::tsquery;", (word,))
        return self.cur.fetchall()

    def read_users_username_includes_phrase(self: phrase: str) -> List[User]:
        self.cur.execute("SELECT id, username, email FROM users WHERE username::tsvector @@ %s::tsquery;", (" <-> ".join(phrase.split()),))
        return self.cur.fetchall()

    def read_user_by_name(self, name: str) -> User:
        self.cur.execute("SELECT id, username, email FROM users WHERE username = %s;", (name,))
        return self.cur.fetchone()

    def delete_user(self, id: int):
        self.cur.execute("DELETE FROM users WHERE id = %s;", (id,))
        self.commit()


    def create_video(self, url: str, owner: int, name: str, public: bool) -> int:
        self.cur.execute("INSERT INTO videos (url, owner, name, public) VALUES (%s, %s, %s, %s) RETURNING id;", (url, owner, name, public))
        self.commit()

        return self.cur.fetchone()[0]

    def update_video_name(self, id: int, name: str):
        self.cur.execute("UPDATE videos SET name = %s WHERE id = %s;", (name, id))
        self.commit()

    def update_video_public(self, id: int, public: bool):
        self.cur.execute("UPDATE videos SET public = %s WHERE is = %s;", (public, id))
        self.commit()

    def read_videos(self) -> List[Video]:
        self.cur.execute("SELECT id, url, owner, name, public, upload_date FROM videos;")
        return self.cur.fetchall()

    def read_video_by_id(self, id: int) -> Video:
        self.cur.execute("SELECT id, url, owner, name, public, upload_date FROM videos WHERE id = %s;", (id,))
        return self.cur.fetchone()

    def read_videos_by_owner(self, owner: int) -> List[Video]:
        self.cur.execute("SELECT id, url, owner, name, public, upload_date FROM videos WHERE owner = %s;", (owner,))
        return self.cur.fetchall()

    def read_videos_by_playlist(self, playlist: int) -> List[Video]:
        self.cur.execute("SELECT v.id, v.url, v.owner, v.name, v.public, v.upload_date FROM videos v INNER JOIN \"video-playlist\" p ON v.id = p.video WHERE p.playlist = %s;", (playlist,))
        return self.cur.fetchall()

    def read_videos_by_public(self, public: bool) -> List[Video]:
        self.cur.execute("SELECT id, url, owner, name, public, upload_date FROM videos WHERE public = %s;", (public,))
        return self.cur.fetchall()

    def read_videos_by_date_range(self, date_range: Tuple[datetime, datetime]) -> List[Video]:
        self.cur.execute("SELECT id, url, owner, name, public, upload_date FROM videos WHERE upload_date BETWEEN SYMMETRIC %s AND %s;", date_range)
        return self.cur.fetchall()

    def delete_video(self, id: int):
        self.cur.execute("DELETE FROM videos WHERE id = %s;", (id,))
        self.commit()


    def create_playlist(self, owner: int, name: str) -> int:
        self.cur.execute("INSERT INTO playlists (owner, name) VALUES(%s, %s) RETURNING id;", (owner, name))
        self.commit()

        return self.cur.fetchone()[0]

    def update_playlist_name(self, id: int, name: str):
        self.cur.execute("UPDATE playlists SET name = %s WHERE id = %s;", (name, id))
        self.commit()

    def read_playlists(self) -> List[Playlist]:
        self.cur.execute("SELECT id, name, owner FROM playlists;")
        return self.cur.fetchall()

    def read_playlist_by_id(self, id: int) -> Playlist:
        self.cur.execute("SELECT id, name, owner FROM playlists WHERE id = %s;", (id,))
        return self.cur.fetchone()

    def read_playlists_by_owner(self, owner: id) -> List[Tuple[int, str, int]]:
        self.cur.execute("SELECT id, name, owner FROM playlists WHERE owner = %s;", (owner,))
        return self.cur.fetchall()

    def delete_playlist(self, id: int):
        self.cur.execute("DELETE FROM playlists WHERE id = %s;", (id,))
        self.commit()

    def add_playlist_video(self, video: int, playlist: int):
        self.cur.execute("INSERT INTO \"video-playlist\" (video, playlist) VALUES (%s, %s)", (video, playlist))
        self.commit()

    def remove_playlist_video(self, video: int, playlist: int):
        self.cur.execute("DELETE FROM \"video-playlist\" WHERE video = %s AND playlist = %s;", (video, playlist))
        self.commit()


    def commit(self):
        self.conn.commit()
