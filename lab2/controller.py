import psycopg2
from model import Model
from view import View

from functools import partial
from typing import Optional, List, Any, Dict, Callable


def transpose(titles: List[str], rows: List[List[Any]]) -> Dict[str, List[Any]]:
    dict = {k: [] for k in titles}

    for i in range(len(titles)):
        for row in rows:
            dict[titles[i]].append(row[i])

    return dict


transpose_users = partial(transpose, ["id", "username", "email"])


transpose_videos = partial(transpose, ["id", "url", "owner", "name", "public", "upload_date"])


transpose_playlists = partial(transpose, ["id", "name", "owner"])


class Controller:
    def __init__(self):
        self.model = Model()
        self.view = View()

        self.view.on("create", "user", self.create_user)
        self.view.on("update", "user", self.update_user)
        self.view.on("delete", "user", self.delete_user)
        self.view.on("show", "user", self.show_users)

        self.view.on("create", "video", self.create_video)
        self.view.on("update", "video", self.update_video)
        self.view.on("delete", "video", self.delete_video)
        self.view.on("show", "video", self.show_videos)

        self.view.on("create", "playlist", self.create_playlist)
        self.view.on("update", "playlist", self.update_playlist)
        self.view.on("delete", "playlist", self.delete_playlist)
        self.view.on("show", "playlist", self.show_playlists)

        self.view.on_add_playlist_video(self.add_playlist_video)
        self.view.on_remove_playlist_video(self.remove_playlist_video)

    def loop(self):
        while True:
            try:
                self.view.wait_for_command()
            except psycopg2.Error as err:
                self.view.show_error(str(err).strip())
            except KeyboardInterrupt:
                return

    def generic_update(self, table: str, fields: Dict[str, Dict]):
        """
        { field_name: { nullable: bool, update: Callable, read: Callable } }
        """
        id = self.view.read_int("enter %s id" % table)
        if id is None:
            return

        while True:
            if len(fields) == 1:
                field = list(fields)[0]
            else:
                field = self.view.read_str("which field to change (%s)?" % "|".join(fields))
                if field == "":
                    return

            if field in fields:
                nullable = fields[field]["nullable"]
                update = fields[field]["update"]
                read = fields[field]["read"]

                if fields[field]["nullable"]:
                    action = self.view.read_str("field is nullable. change or delete it?")
                    if action == "delete":
                        update(id, None)
                        self.view.show_str("successfully deleted %s" % field)
                    elif action == "change":
                        value = read("enter new %s" % field)
                        if value is None or value == "":
                            return
                        update(id, value)
                        self.view.show_str("successfully updated %s" % field)
                    else:
                        self.view.show_error("invalid action")
                else:
                    value = read("enter new %s" % field)
                    if value is None or value == "":
                        return
                    update(id, value)
                    self.view.show_str("successfully updated %s" % field)
            else:
                self.view.show_error("invalid field")

    def create_user(self):
        username = self.view.read_str("enter username")
        if username == "":
            self.view.show_error("Username can not be empty")
            return
        email = self.view.read_str("enter email")

        id = self.model.create_user(username, email or None)
        self.view.show_table("user", {"id": [id], "username": [username], "email": [email]})

    def update_user(self):
        self.generic_update("user", {
            "username": {
                "nullable": False,
                "update": self.model.update_user_username,
                "read": self.view.read_str
            },
            "email": {
                "nullable": True,
                "update": self.model.update_user_email,
                "read": self.view.read_str
            }
        })


    def delete_user(self):
        id = self.view.read_int("enter user id to delete")
        if id is None:
            return
        self.model.delete_user(id)
        self.view.show_str("deletion successful")

    def show_users(self, field: Optional[str]):
        if field is None:
            self.view.show_table("users", transpose_users(self.model.read_users()))
        elif field == "id":
            id = self.view.read_int("enter user id")
            if id is None:
                return
            self.view.show_table(
                "user %s" % id,
                transpose_users([self.model.read_user_by_id(id)])
            )
        elif field == "username":
            username = self.view.read_str("enter username")
            if username is None:
                return
            self.view.show_table(
                "user %s" % username,
                transpose_users([self.model.read_user_by_name(username)])
            )
        elif field == "username not includes":
            word = self.view.read_str("enter word to exclude")
            if word == '':
                return
            self.view.show_table("users", transpose_users(self.model.read_users_username_not_includes(word)))
        else:
            self.view.show_error("invalid field")

    def create_video(self):
        owner = self.view.read_int("enter owner")
        if owner is None:
            return
        url = self.view.read_str("enter video url")
        if url == "":
            return
        name = self.view.read_str("enter video name")
        if name == "":
            return
        public = self.view.read_bool("make it public?")
        if public is None:
            return

        id = self.model.create_video(url, owner, name, public)
        self.view.show_table("video", {
            "id": [id],
            "url": [url],
            "owner": [owner],
            "name": [name],
            "public": [public]
        })

    def update_video(self):
        self.generic_update("video", {
            "name": {
                "nullable": False,
                "update": self.model.update_video_name,
                "read": self.view.read_str
            },
            "public": {
                "nullable": False,
                "update": self.model.update_video_public,
                "read": self.view.read_bool
            }
        })

    def delete_video(self):
        id = self.view.read_int("enter video id to delete")
        if id is None:
            return
        self.model.delete_video(id)
        self.view.show_str("deletion successful")

    def show_videos(self, field: Optional[str]):
        if field is None:
            self.view.show_table("videos", transpose_videos(self.model.read_videos()))
        elif field == "id":
            id = self.view.read_int("enter video id")
            if id is None:
                return
            self.view.show_table(
                "video %s" % id,
                transpose_videos([self.model.read_video_by_id(id)])
            )
        elif field == "owner":
            owner = self.view.read_int("enter user id")
            if owner is None:
                return
            self.view.show_table("%s's videos", transpose_videos(self.model.read_videos_by_owner(owner)))
        elif field == "playlist":
            playlist = self.view.read_int("enter playlist id")
            if playlist is None:
                return
            self.view.show_table("videos from playlist %s" % playlist, transpose_videos(self.model.read_videos_by_playlist(playlist)))
        elif field == "public":
            public = self.view.read_bool("show public videos?")
            if public is None:
                return
            if public:
                line = "public videos"
            else:
                line = "private videos"
            self.view.show_table(line, transpose_videos(self.model.read_videos_by_public(public)))
        elif field == "date":
            date_range = self.view.read_date_range("enter upload date range")
            if date_range is None:
                return
            line = "videos uploaded between %s and %s" % date_range
            self.view.show_table(line, transpose_videos(self.model.read_videos_by_date_range(date_range)))
        else:
            self.view.show_error("invalid field")


    def create_playlist(self):
        owner = self.view.read_int("enter owner")
        if owner is None:
            return
        name = self.view.read_str("enter playlist name")
        if name == "":
            return

        id = self.model.create_playlist(owner, name)
        self.view.show_table("playlist", {
            "id": [id],
            "name": [name],
            "owner": [owner]
        })

    def update_playlist(self):
        self.genetic_update("playlist", {
            "name": {
                "nullable": False,
                "update": self.model.update_playlist_name,
                "read": self.view.read_str
            }
        })

    def delete_playlist(self):
        id = self.view.read_int("enter playlist id to delete")
        if id is None:
            return
        self.model.delete_playlist(id)
        self.view.show_str("deletion successful")

    def show_playlists(self, field: Optional[str]):
        if field is None:
            self.view.show_table("playlists", transpose_playlists(self.model.read_playlists()))
        elif field == "id":
            id = self.view.read_int("enter playlist id")
            if id is None:
                return
            self.view.show_table(
                "playlist %s" % id,
                transpose_playlists([self.model.read_playlist_by_id(id)])
            )
        elif field == "owner":
            owner = self.view.read_int("enter owner id")
            if owner is None:
                return
            self.view.show_table(
                "%s's playlists" % owner,
                transpose_playlists(self.model.read_playlists_by_owner(owner))
            )
        else:
            self.view.show_error("invalid field")

    def add_playlist_video(self):
        playlist = self.view.read_int("enter playlist id")
        if playlist is None:
            return
        video = self.view.read_int("enter video id")
        if video is None:
            return

        self.model.add_playlist_video(video, playlist)
        self.view.show_str("added video to playlist")

    def remove_playlist_video(self):
        playlist = self.view.read_int("enter playlist id")
        if playlist is None:
            return
        video = self.view.read_int("enter video id")
        if video is None:
            return

        self.model.remove_playlist_video(video, playlist)
        self.view.show_str("removed video from playlist")
