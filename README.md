
Будьонний Павло  
КП-72

## Lab 1: Ознайомлення з базовими операціями СУБД PostgreSQL

[ER діаграма](lab1/ER.png)

[Структурна діаграма](lab1/Struct.png)


## Lab2: Ознайомлення з базовими операціями СУБД PostgreSQL

Варіант 5

[screenshot1](lab2/screenshot1.png)  
[screenshot2](lab2/screenshot2.png)

[Структурна діаграма](lab2/Struct.png)


## Lab 3: Засоби оптимізації роботи СУБД PostgreSQL

Демонстрація рівнів ізоляції транцакцій:

[Транзакція 1](lab3/mut.png)  
[read committed](lab3/1.png)  
[repeatable read](lab3/2.png)  
