import psycopg2
import sqlalchemy
from sqlalchemy import Column, Integer, Text, Table, MetaData, types, Boolean

from datetime import datetime
from typing import Dict, List, Optional, Union, Tuple


User = Tuple[int, str, str]
Video = Tuple[int, str, int, str, bool, datetime]
Playlist = Tuple[int, str, int]



class Model:
    def __init__(self):
        self.engine = sqlalchemy.create_engine("postgres://postgres:postgres@localhost/Lab")
        self.meta = MetaData()

        self.users = Table(
            'users', self.meta,
            Column('id', Integer, primary_key=True),
            Column('username', Text),
            Column('email', Text)
        )

        self.videos = Table(
            'videos', self.meta,
            Column('id', Integer, primary_key=True),
            Column('url', Text),
            Column('owner', Integer),
            Column('name', Text),
            Column('upload_date', types.DateTime),
            Column('public', Boolean)
        )

        self.playlists = Table(
            'playlists', self.meta,
            Column('id', Integer, primary_key=True),
            Column('name', Text),
            Column('owner', Integer)
        )

        self.vp = Table(
            'video_playlist', self.meta,
            Column('video', Integer, primary_key=True),
            Column('playlist', Integer, primary_key=True)
        )


    def create_user(self, username: str, email: Optional[str]) -> int:
        stmt = self.users \
            .insert() \
            .values(username=username, email=email)

        self.engine.execute(stmt).fetchone()['id']

    def update_user_username(self, id: int, username: str):
        stmt = self.users \
            .update() \
            .where(self.users.c.id == id) \
            .values(username=username)

        self.engine.execute(stmt)

    def update_user_email(self, id: int, email: Optional[str]):
        stmt = self.users \
            .update() \
            .where(self.users.c.id == id) \
            .values(email=email)

        self.engine.execute(stmt)

    def read_users(self) -> List[User]:
        stmt = self.users.select()

        return self.engine.execute(stmt).fetchall()

    def read_user_by_id(self, id: int) -> User:
        stmt = self.users.select().where(self.users.c.id == id)

        return self.engine.execute(stmt).fetchone()

    def read_user_by_name(self, name: str) -> User:
        stmt = self.users.select().where(self.users.c.username == name)

        return self.engine.execute(stmt).fetchone()

    def delete_user(self, id: int):
        stmt = self.users.delete().where(self.users.c.id == id)

        self.engine.execute(stmt)

    def create_video(self, url: str, owner: int, name: str, public: bool) -> int:
        stmt = self.videos \
            .insert() \
            .values(url=url, owner=owner, name=name, public=public)

        return self.engine.execute(stmt).fetchone()['id']

    def update_video_name(self, id: int, name: str):
        stmt = self.videos \
            .update() \
            .where(self.videos.c.id == id) \
            .values(name=name)

        self.engine.execute(stmt)

    def update_video_public(self, id: int, public: bool):
        stmt = self.videos \
            .update() \
            .where(self.videos.c.id == id) \
            .values(public=public)

        self.engine.execute(stmt)

    def read_videos(self) -> List[Video]:
        stmt = self.videos.select()

        return self.engine.execute(stmt).fetchall()

    def read_video_by_id(self, id: int) -> Video:
        stmt = self.videos.select().where(self.videos.c.id == id)

        return self.engine.execute(stmt).fetchone()

    def read_videos_by_owner(self, owner: int) -> List[Video]:
        stmt = self.videos.select().where(self.videos.c.owner == owner)

        return self.engine.execute(stmt).fetchall()

    def read_videos_by_playlist(self, playlist: int) -> List[Video]:
        stmt = sqlalchemy \
            .select([self.videos]) \
            .select_from(self.videos.join(self.vp, self.videos.c.id == self.vp.c.video)) \
            .where(self.vp.c.playlist == playlist)

        return self.engine.execute(stmt).fetchall()

    def read_videos_by_public(self, public: bool) -> List[Video]:
        stmt = self.videos.select().where(self.videos.c.public == public)

        return self.engine.execute(stmt).fetchall()

    def read_videos_by_date_range(self, date_range: Tuple[datetime, datetime]) -> List[Video]:
        stmt = self.videos.select().where(sqlalchemy.between(self.videos.c.upload_date, date_range[0], date_range[1]))

        return self.engine.execute(stmt).fetchall()

    def delete_video(self, id: int):
        stmt = self.videos.delete().where(self.videos.c.id == id)

        self.engine.execute(stmt)


    def create_playlist(self, owner: int, name: str) -> int:
        stmt = self.playlists.insert().values(owner=owner, name=name)

        return self.engine.execute(stmt).fetchone()['id']

    def update_playlist_name(self, id: int, name: str):
        stmt = self.playlists.update().where(self.playlists.c.id == id).values(name=name)

        self.engine.execute(stmt)

    def read_playlists(self) -> List[Playlist]:
        stmt = self.playlists.select()

        return self.engine.execute(stmt).fetchall()

    def read_playlist_by_id(self, id: int) -> Playlist:
        stmt = self.playlists.select().where(self.playlists.c.id == id)

        return self.engine.execute(stmt).fetchone()

    def read_playlists_by_owner(self, owner: id) -> List[Tuple[int, str, int]]:
        stmt = self.playlists.select().where(self.playlists.c.owner == owner)

        return self.engine.execute(stmt).fetchall()

    def delete_playlist(self, id: int):
        stmt = self.playlists.delete().where(self.playlists.c.id == id)

        self.engine.execute(stmt)

    def add_playlist_video(self, video: int, playlist: int):
        stmt = self.vp.insert().values(video=video, playlist=playlist)

        self.engine.execute(stmt)

    def remove_playlist_video(self, video: int, playlist: int):
        stmt = self.vp.delete().where(self.vp.video == video and self.vp.playlist == playlist)

        self.engine.execute(stmt)
