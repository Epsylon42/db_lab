import psycopg2
import random
import dateparser
from datetime import datetime

from model import Model


def gen_str(range_from, range_to) -> str:
    letters = [chr(x) for x in range(ord('a'), ord('z')+1)]

    length = random.randint(range_from, range_to)
    return ''.join([random.choice(letters) for _ in range(length)])


model = Model()
user_ids = [user[0] for user in model.read_users()]

ext = ["com", "net", "org"]
for _ in range(60000):
    url = "https://%s.%s/%s" % (gen_str(3, 7), random.choice(ext), gen_str(5, 10))
    owner = random.choice(user_ids)
    name = gen_str(5, 15)
    public = random.randint(0, 1) == 0

    upload_date = dateparser.parse("%s years ago" % random.randrange(1, 10))
    stmt = model.videos.insert().values(url=url, owner=owner, name=name, public=public, upload_date=upload_date)
    model.engine.execute(stmt)
