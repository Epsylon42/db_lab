#!/usr/bin/zsh

alias time="/usr/bin/time -f '%E'"

echo 'select all videos'
echo 'select * from videos;' | time psql Lab > /dev/null

echo 'select videos where owner in range'
echo 'select * from videos where owner > 46219 and owner < 50000;' | time psql Lab > /dev/null

echo 'select videos from .net urls'
echo "select * from videos where url like '%.net%' and name like '%abc%' and public = true;" | time psql Lab > /dev/null


echo 'create btree index'
echo 'create index videos_btree on videos using btree (owner);' | time psql Lab > /dev/null

echo 'select video where owner in range (indexed)'
echo 'select * from videos where owner > 46219 and owner < 50000;' | time psql Lab > /dev/null

echo 'drop btree index'
echo 'drop index videos_btree;' | psql Lab > /dev/null


echo 'create gin index'
echo 'create index videos_gin on videos using gin (url gin_trgm_ops, name gin_trgm_ops);' | time psql Lab > /dev/null

echo 'select videos from .net urls (indexed)'
echo "select * from videos where url like '%.net%' and name like '%abc%' and public = true;" | time psql Lab > /dev/null

echo 'drop gin index'
echo 'drop index videos_gin;' | psql Lab > /dev/null
